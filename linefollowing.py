#!/usr/bin/python3.4

import ev3dev.ev3 as ev3
from time import sleep

SENSOR_THRESHOLD = 50   # >60 = no line (white)
BASE_SPEED = 60         #Speed of the robot "Duty-cycle 0-100"
SPEED_DEF = 10          #Wheel speed diffrence when correcting path "Duty-cycle 0-100"

class Linefollower:
    def __init__(self):
        self.btn = ev3.Button()
        self.shut_down = False

    def follow_line(self):
        left_motor = ev3.LargeMotor('outA');    assert left_motor.connected
        right_motor = ev3.LargeMotor('outB');    assert right_motor.connected
        right_motor.run_direct()
        left_motor.run_direct()

        left_sensor = ev3.ColorSensor('in1');    assert left_sensor.connected
        left_sensor.mode = 'COL-REFLECT'        
        right_sensor = ev3.ColorSensor('in2');   assert right_sensor.connected
        right_sensor.mode = 'COL-REFLECT'

        #self.gyro = ev3.GyroSensor('in3')
        #self.gyro.mode = 'GYRO-ANG'
        #gyro.reset()
        
        #print("Press ENTER to start")
        #while True:  
        #    if self.btn.enter:
        #        break

        while not self.shut_down:
            left_sensor_val = left_sensor.value()
            right_sensor_val = right_sensor.value()
            if (left_sensor_val > SENSOR_THRESHOLD) and (right_sensor_val > SENSOR_THRESHOLD)   :
                left_motor.duty_cycle_sp = BASE_SPEED
                right_motor.duty_cycle_sp = BASE_SPEED
                text = "Straight"
            elif (left_sensor_val < SENSOR_THRESHOLD) and (right_sensor_val > SENSOR_THRESHOLD)   :
                left_motor.duty_cycle_sp = BASE_SPEED - SPEED_DEF
                right_motor.duty_cycle_sp = BASE_SPEED
                text = "Going left"
            elif (left_sensor_val > SENSOR_THRESHOLD) and (right_sensor_val < SENSOR_THRESHOLD)   :
                left_motor.duty_cycle_sp = BASE_SPEED
                right_motor.duty_cycle_sp = BASE_SPEED - SPEED_DEF
                text = "Going right"
            else:
                text = "Cross"
                left_motor.duty_cycle_sp = -BASE_SPEED/2
                right_motor.duty_cycle_sp = BASE_SPEED/2
                while(left_sensor_val < SENSOR_THRESHOLD) and (right_sensor_val < SENSOR_THRESHOLD):
                    left_sensor_val = left_sensor.value()
                    right_sensor_val = right_sensor.value()
                while(left_sensor_val > SENSOR_THRESHOLD) and (right_sensor_val > SENSOR_THRESHOLD):
                    left_sensor_val = left_sensor.value()
                    right_sensor_val = right_sensor.value()



            print("Leftsensor:",left_sensor_val,"Rightsensor:",right_sensor_val, text)

            if  self.btn.backspace:  # Stop
                print("Exit program... ")
                left_motor.stop()
                right_motor.stop()
                self.shut_down = True



if __name__ == "__main__":
    robot = Linefollower()
    robot.follow_line()