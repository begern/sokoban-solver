#!/usr/bin/python3.4

import ev3dev.ev3 as ev3
from time import time,sleep
from ev3dev.ev3 import *

class Linefollower:
    
    def __init__(self):
        self.btn = ev3.Button()
        self.shut_down = False

    def follow_line(self):
        right_motor = ev3.LargeMotor('outA');    assert right_motor.connected
        left_motor = ev3.LargeMotor('outC');    assert left_motor.connected
        left_motor.run_direct()
        right_motor.run_direct()
        left_motor.duty_cycle_sp = 0
        right_motor.duty_cycle_sp = 0

        follow_sensor = ev3.ColorSensor('in1');    assert follow_sensor.connected
        follow_sensor.mode = 'COL-REFLECT'        
        cross_sensor = ev3.ColorSensor('in2');   assert cross_sensor.connected
        cross_sensor.mode = 'COL-REFLECT'
        
        #print("Press ENTER to start")
        #while True:  
        #    if self.btn.enter:
        #        break
        
        my_sound = Sound()

        power = 50
        minRef = 2      #Sensor value on black line
        maxRef = 71     #Sensor value on paper
        target = 55
        kp = float(0.5)
        kd = float(0.8)
        ki = float(0.02)
        direction = -1
        lastError = error = integral = 0

        path = "fblbss"
        curr_dir = path[0]
        i = 0

        cross_detected = 0
        CROSS_SENSOR_LIMIT = 20

        while not self.shut_down:
            
            refRead = follow_sensor.value()
            error = target - (100*(refRead-minRef)/(maxRef-minRef))
            derivative = error - lastError
            lastError = error
            integral = float(0.5) * integral + error
            course = (kp * error + kd * derivative +ki * integral) * direction

            if course >= 0:
                if course > 100:
                    power_right = 0
                    power_left = power
                else:	
                    power_left = power
                    power_right = power - ((power * course) / 100)
            else:
                if course < -100:
                    power_left = 0
                    power_right = power
                else:
                    power_right = power
                    power_left = power + ((power * course) / 100)
            
            left_motor.duty_cycle_sp = power_left
            right_motor.duty_cycle_sp = power_right

            if cross_sensor.value() < CROSS_SENSOR_LIMIT:
                cross_detected += 1
                print("CROSS")
                while cross_sensor.value() < CROSS_SENSOR_LIMIT: 
                    pass
                if (curr_dir == "l") or (curr_dir == "r"):
                    sleep(0.7)
                    if curr_dir == "l":        #Left turn
                        left_motor.duty_cycle_sp = -30
                        right_motor.duty_cycle_sp = 30
                        print("TURNING LEFT")
                        sleep(0.2)
                        while follow_sensor.value() > CROSS_SENSOR_LIMIT:
                            pass
                        while follow_sensor.value() < CROSS_SENSOR_LIMIT:
                            pass
                        lastError = error = integral = 0
                    if curr_dir == "r":
                        left_motor.duty_cycle_sp = 30
                        right_motor.duty_cycle_sp = -30
                        print("TURNING RIGHT")
                        sleep(0.2)
                        while cross_sensor.value() > CROSS_SENSOR_LIMIT:    #As long as the cross sensor is on white
                            pass
                        sleep(0.2)
                        lastError = error = integral = 0
                if curr_dir == "b":
                    left_motor.duty_cycle_sp = -25
                    right_motor.duty_cycle_sp = -25
                    print("GOING BACK")
                    sleep(0.65)
                    left_motor.duty_cycle_sp = 30
                    right_motor.duty_cycle_sp = -30
                    print("U-TURN")
                    sleep(0.3)
                    while cross_sensor.value() > CROSS_SENSOR_LIMIT:    #As long as the cross sensor is on white
                        pass
                    sleep(0.4)
                i += 1
                if i < len(path): 
                    curr_dir = path[i]
                else: 
                    self.shut_down = True
                    left_motor.stop()
                    right_motor.stop()
                    print("Done running")
                    my_sound.tone(1500, 500).wait()

            sleep(0.01) #100Hz

            if  self.btn.backspace:  # Stop
                print("Exit program... ")
                left_motor.stop()
                right_motor.stop()
                self.shut_down = True



if __name__ == "__main__":
    robot = Linefollower()
    robot.follow_line()