#/usr/bin/python3

import curses

screen = curses.initscr()

player = [0,0]
map = []
mapIdx = 1
mapCount = 9

class Field:
    TYPE_ERROR = 0
    TYPE_FLOOR = 1
    TYPE_WALL  = 2

    isBoxOn = False
    isPlayer = False
    isGoal = False
    boxType = TYPE_ERROR

    def __init__(self, char):
        if char is '#':  self.boxType = Field.TYPE_WALL
        if char is ' ':  self.boxType = Field.TYPE_FLOOR
        if char is '.':  
            self.boxType = Field.TYPE_FLOOR
            self.isGoal = True
        if char is 's':  
            self.boxType = Field.TYPE_FLOOR
            self.isPlayer = True
        if char is 'm':
            self.boxType = Field.TYPE_FLOOR
            self.isBoxOn = True


    def getChar(self):
        if self.boxType is Field.TYPE_WALL: return '▓'
        if self.isPlayer: return 'o'
        if self.isBoxOn: return '■'
        if self.isGoal: return '.'
        if self.boxType is Field.TYPE_ERROR: return 'Ø'
        return ' '
    
def loadMap(mapId):
    global map
    global player

    mapFile = open("maps/lvl_{:03d}.smp".format(mapId),'r')

    lines = mapFile.readlines()

    for y in range(len(lines)):
        if lines[y][-1] is '\n': lines[y] = lines[y][:-1]
        for x in range(len(lines[y])):
            if lines[y][x] is 's': 
                player[0] = x
                player[1] = y
    
    map = [[Field(x) for x in l] for l in lines]

loadMap(1)


screen.addstr("SOKOBAN!\nMove with arrow keys\n[j] and [k] Change level\n[r] restart level\n[q] quit\n\n[Press any key to start]\n")

def move(dx, dy):
    global player

    x = player[0]+dx
    y = player[1]+dy
    if map[y][x].boxType is Field.TYPE_FLOOR:
        if map[y][x].isBoxOn:
            if map[y+dy][x+dx].boxType is Field.TYPE_FLOOR:
                if not map[y+dy][x+dx].isBoxOn:
                    map[y][x].isPlayer = True
                    map[player[1]][player[0]].isPlayer = False
                    map[y][x].isBoxOn = False
                    map[y+dy][x+dx].isBoxOn = True
                    player[0] += dx
                    player[1] += dy

            
        else:
            map[y][x].isPlayer = True
            map[player[1]][player[0]].isPlayer = False
            player[0] += dx
            player[1] += dy


while True:
    ch = screen.getch()
    screen.clear()

    if ch is 113: break

    if ch is 68 or ch is 97: 
        screen.addstr("left:  ") #LEFT
        move(-1,0)

    if ch is 67 or ch is 100: 
        screen.addstr("right: ") #RIGHT
        move(1,0)

    if ch is 65 or ch is 119: 
        screen.addstr("up:   ") #UP
        move(0,-1)

    if ch is 66  or ch is 115: 
        screen.addstr("down: ") #DOWN
        move(0,1)

    if ch is 114: #RELOAD
        loadMap(mapIdx)
    
    if ch is 106: #NEXT
        if mapIdx is mapCount: mapIdx = 1
        else: mapIdx += 1
        loadMap(mapIdx)
        
    if ch is 107: #NEXT
        if mapIdx is 1: mapIdx = mapCount
        else: mapIdx -= 1
        loadMap(mapIdx)
    
    screen.addstr("[%d,%d]\n" % (player[0],player[1]))
    
    for line in map:
        for c in line:
            screen.addch(c.getChar())
        screen.addch('\n')
    screen.refresh()

curses.endwin()

